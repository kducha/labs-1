﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMałpa);

        public static Type ISub1 = typeof(IGoryl);
        public static Type Impl1 = typeof(Impl1);

        public static Type ISub2 = typeof(IPawian);
        public static Type Impl2 = typeof(Impl2);


        public static string baseMethod = "Metoda";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "MetodaISub1";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "MetodaISub2";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Stworz";
        public static string collectionConsumerMethod = "RobCos";

        #endregion

        #region P3

        public static Type IOther = typeof(string);
        public static Type Impl3 = typeof(string);

        public static string otherCommonMethod = "...";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
