﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    class Impl2 : IPawian, IGoryl
    {
        public Impl2()
        { }

        public int MetodaGoryl()
        {
            return 5;
        }

        public int MetodaISub1()
        {
            return 4;
        }

        public int Metoda()
        {
            return 5;
        }

        public int MetodaISub2()
        {
            return 6;
        }

        int IGoryl.MetodaGoryl()
        {
            throw new NotImplementedException();
        }

        int IGoryl.MetodaISub1()
        {
            throw new NotImplementedException();
        }

        int IMałpa.Metoda()
        {
            return -1;
        }

        int IPawian.MetodaISub2()
        {
            throw new NotImplementedException();
        }
    }
}
