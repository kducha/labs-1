﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;

namespace Lab1.Main
{


    public class Program
    {
        

        static void Main(string[] args)
        {
            Program p = new Program();

            IList<IMałpa> lista = p.Stworz();

            p.RobCos(lista);
            
        }

        public void RobCos(IList<IMałpa> lista)
        {
            foreach (IMałpa malpa in lista)
            {
                Console.WriteLine(malpa.Metoda());
            }
        }

        public IList<IMałpa> Stworz()
        {
            return (IList<IMałpa>)(new List<IMałpa>() { new Impl1(), new Impl2(), });
        }
    }
}
